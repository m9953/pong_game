#pragma once

// Define default window size.
#define WIDTH 720
#define HEIGHT 720
#define FPS 60.0

// Font
#define FONTSIZE 24
#define FONTPATH "joystix monospace.ttf"

// Define paddle parameters.
#define PADDLEWIDTH 10
#define PADDLEHEIGHT 100
#define PADDLESPEED 18.0

// Define ball parameters.
#define BALLRADIUS 5
#define BALLSPEED 30.0

// Define scoreboard parameters.
#define SWIDTH 60
#define SHEIGHT 30