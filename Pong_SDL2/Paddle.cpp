#include "Paddle.h"

Paddle::Paddle(int x, int y) {
	// Set position of the paddle.
	position.x = x;
	position.y = y;

	yCoord = static_cast<double>(y);

	// Set dimentions of the paddle.
	position.w = PADDLEWIDTH;
	position.h = PADDLEHEIGHT;

	// Set color as white.
	color.r = 255;
	color.g = 255;
	color.b = 255;
	color.a = 255;

	// Default direction = 0 (no movement).
	direction = 0;
}

void Paddle::SetDirection(int newDir) {
	if (newDir > 1 || newDir < -1) {
		std::cout << "Wrong direction parameter set. New directon = 0." << std::endl;
		direction = 0;
	}
	else  {
		if (position.y == HEIGHT - PADDLEHEIGHT && newDir == -1) direction = 0;
		else if (position.y == 0 && newDir == 1) direction = 0;
		else direction = newDir;
	}
}

SDL_Rect* Paddle::GetPosition() {
	return &position;
}

void Paddle::Update(double time) {
	yCoord -= (double)direction * PADDLESPEED * time;
	position.y = yCoord;

	// Prevent self movement.
	SetDirection(0);
}

void Paddle::Render(SDL_Renderer *renderer) {
	// Dye the paddle.
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	SDL_RenderFillRect(renderer, &position);
}

void Paddle::SetPositionY(int y) {
	position.y = y;
	yCoord = y;
}
