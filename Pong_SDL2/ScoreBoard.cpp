#include "ScoreBoard.h"

ScoreBoard::ScoreBoard(): left(0), right(0) {
    // Place scoreboard on a scene.
    position.x = WIDTH/2 - SWIDTH/2;
    position.y = 0;

    position.w = SWIDTH;
    position.h = SHEIGHT;

    // Set color as white.
    textColor.r = 255;
    textColor.b = 255;
    textColor.a = 255;
    textColor.g = 255;
}

ScoreBoard::ScoreBoard(int x, int y): left(0), right(0) {
    // Place scoreboard on a scene.
    position.x = x;
    position.y = y;

    position.w = SWIDTH;
    position.h = SHEIGHT;

    // Set color as white.
    textColor.r = 255;
    textColor.b = 255;
    textColor.a = 255;
    textColor.g = 255;
}

void ScoreBoard::Update(int ballPositionX) {
    // If ball is on the right side - add point to the left player
    if (ballPositionX > WIDTH - PADDLEWIDTH) {
        ++left;
    }
    // If ball is on the left side - add point to the right player
    else if (ballPositionX < PADDLEWIDTH) {
        ++right;
    }
}

void ScoreBoard::Render(SDL_Renderer* renderer, TTF_Font* font) {
    // Score text in format - "left" : "right"
    std::string text = std::to_string(left) + " : " + std::to_string(right);

    SDL_Surface* surface = TTF_RenderText_Solid(font, text.c_str(), textColor);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    
    // Draw the scoreboard.
    SDL_RenderCopy(renderer, texture, NULL, &position);
    
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
}
