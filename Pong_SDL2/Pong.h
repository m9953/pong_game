#pragma once

#include <iostream>
#include <SDL.h>

#include "Paddle.h"
#include "Ball.h"
#include "ScoreBoard.h"

class Pong
{
public:
	Pong();
	~Pong() = default;

	void GameMainLoop();
	void Update(double time);
	void Input();
	void Render();

private:
	SDL_Renderer* renderer;
	SDL_Window* window;
	SDL_Event event;
	SDL_Color BGColor;
	TTF_Font* font;

	bool isRunning;

	Paddle left_paddle, right_paddle;
	Ball ball;
	ScoreBoard score;

	void CheckCollisions();
};
