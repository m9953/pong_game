#pragma once

#include <string>
#include <SDL.h>
#include <SDL_ttf.h>

#include "Parameters.h"

class ScoreBoard
{
public:
	ScoreBoard();
	ScoreBoard(int x, int y);
	~ScoreBoard() = default;

	void Update(int ballPositionX);
	void Render(SDL_Renderer* renderer, TTF_Font* font);

private:
	// Score of the LEFT/RIGHT player.
	unsigned int left;
	unsigned int right;

	// Position of the ScoreBoard.
	SDL_Rect position;
	// Font color.
	SDL_Color textColor;
};

