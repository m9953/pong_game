#pragma once
#include <SDL.h>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <cmath>

#define _USE_MATH_DEFINES

#include "Parameters.h"

class Ball
{
public:
    Ball();
    Ball(int x, int y);
    ~Ball() = default;

    SDL_Rect* GetPosition();
    void Bounce(SDL_Rect* paddle);

    void Update(double time);
    void Render(SDL_Renderer* renderer);

    bool isAlive;

private:
    SDL_Rect position;
    SDL_Color color;

    double xCoord;
    double yCoord;  

    // Direction of ball movement.
    std::vector<double> direction;

    void CheckCollisions();
};
