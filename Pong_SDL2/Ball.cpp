#include "Ball.h"

Ball::Ball() {
    position.x = 0;
    position.y = 0;
    position.w = BALLRADIUS * 2;
    position.h = BALLRADIUS * 2;

    direction = { 1, 1 };

    isAlive = 1;

    color.r = 255;
    color.g = 255;
    color.b = 255;
    color.a = 255;
}

Ball::Ball(int x, int y) {
    // Place ball on a scene.
    position.x = x;
    position.y = y;

    position.w = BALLRADIUS * 2;
    position.h = BALLRADIUS * 2;

    xCoord = position.x;
    yCoord = position.y;

    // Give a ball random direction of movement.
    double angle;
    do {
        angle = (double)(rand() % 2000) / 1000;
    } 
    // Prevent bouncing from top to bottom too much from the very beginning.
    while (abs(1 - angle) > 0.3 && abs(1 - angle) < 0.6);

    direction = { cos(angle * M_PI), sin(angle * M_PI) };

    isAlive = 1;

    // Set color as white.
    color.r = 255;
    color.g = 255;
    color.b = 255;
    color.a = 255;
}

SDL_Rect* Ball::GetPosition() {
    return &position;
}

void Ball::Bounce(SDL_Rect* paddle) {
    double rel = (paddle->y + (PADDLEHEIGHT / 2)) - (position.y + BALLRADIUS);
    double norm = rel / (PADDLEHEIGHT / 2);
    double bounce = norm * (5 * M_PI / 2);
    direction[0] = cos(bounce);

    if (paddle->x > WIDTH / 2) {
        direction[0] = -direction[0];
    }

    direction[1] = sin(bounce);
}

void Ball::Update(double time) {
    CheckCollisions();

    xCoord += direction[0] * BALLSPEED * time;
    yCoord += direction[1] * BALLSPEED * time;

    position.x = xCoord;
    position.y = yCoord;
}

void Ball::Render(SDL_Renderer* renderer){
    // Dye the ball.
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderFillRect(renderer, &position);
}

void Ball::CheckCollisions() {
    // Hit BOTTOM and UPPER border.
    if (position.y + BALLRADIUS * 2 + direction[1] >= HEIGHT || position.y + direction[1] <= 0) {
        direction[1] = -direction[1];
    }

    // Hit RIGHT and LEFT border
    // In future - split to 2 different conditions and calculate score. Ball should be destroyed.
    if (position.x + BALLRADIUS * 2 > WIDTH || position.x < 0) {
        isAlive = 0;
        direction[0] = 0;
        direction[1] = 0;
    }
}
