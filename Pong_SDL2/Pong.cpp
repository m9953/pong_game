#include "Pong.h"

Pong::Pong(): 
    left_paddle(0, ((HEIGHT / 2) - PADDLEHEIGHT / 2)), 
    right_paddle((WIDTH - PADDLEWIDTH), ((HEIGHT / 2) - PADDLEHEIGHT / 2))
{
    // Trying to init all SDL modules.
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::cout << "Failed to init SDL." << std::endl;
        //maybe add some exceptions here
    }
    
    // Trying to init TTF.
    if (TTF_Init() != 0) {
        std::cout << "Couldn't initialize TTF lib: " << TTF_GetError() << std::endl;
    }

    // Trying to create window that has default width and height, but is resizable.
    if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_RESIZABLE, &window, &renderer)) {
        std::cout << "Failed to create SDL window and renderer." << std::endl;
        //maybe add some exceptions here
    }

    if (SDL_RenderSetLogicalSize(renderer, WIDTH, HEIGHT)) {
        std::cout << "Failed to set render logical size." << std::endl;
        //maybe add some exceptions here
    }

    // Init ball in the center.
    srand(time(NULL));
    ball = Ball( WIDTH / 2 - BALLRADIUS, HEIGHT / 2 - BALLRADIUS);

    score = ScoreBoard(WIDTH / 2 - SWIDTH / 2, 0);

    font = TTF_OpenFont(FONTPATH, FONTSIZE);
    if (!font) {
        printf("TTF_OpenFont: %s\n", TTF_GetError());
    }

    // Set BGColor black.
    BGColor.r = 0;
    BGColor.g = 0;
    BGColor.b = 0;
    BGColor.a = 255;
}

void Pong::GameMainLoop() {
    // Set game state as running.
    isRunning = true;

    // Game main work.
    while (isRunning) {
        Update(1.0 / FPS);
        Input();
        Render();
    }

    // Close the game.
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Pong::Update(double time) {
    CheckCollisions();

    if (!ball.isAlive) {
        score.Update(ball.GetPosition()->x);

        // Reset scene.
        ball = Ball(WIDTH / 2 - BALLRADIUS, HEIGHT / 2 - BALLRADIUS);
        left_paddle.SetPositionY(((HEIGHT / 2) - PADDLEHEIGHT / 2));
        right_paddle.SetPositionY(((HEIGHT / 2) - PADDLEHEIGHT / 2));

        return;
    }

    // Update info for paddles
    // Player`s paddle.
    left_paddle.Update(time);

    // Paddle of the bot.
    // If ball is above the paddle center.
    if (ball.GetPosition()->y + BALLRADIUS - right_paddle.GetPosition()->y < PADDLEHEIGHT / 2) {
        right_paddle.SetDirection(1);
    }
    // If ball is under the paddle center.
    else if (ball.GetPosition()->y + BALLRADIUS - right_paddle.GetPosition()->y > PADDLEHEIGHT / 2) {
        right_paddle.SetDirection(-1);
    }
    // If the ball is on the paddle height. 
    else {
        right_paddle.SetDirection(0);
    }

    right_paddle.Update(time);
    
    // Update info for ball
    ball.Update(time);
}

void Pong::Input() {
    const Uint8* keystates = SDL_GetKeyboardState(NULL);

    while (SDL_PollEvent(&event)) {
        // Check if user closes the window.
        if (event.type == SDL_QUIT) {
            isRunning = false;
        }
    }

    // Check keypresses.
    if (keystates[SDL_SCANCODE_ESCAPE]) isRunning = false;

    if (keystates[SDL_SCANCODE_UP]) {
        left_paddle.SetDirection(1);
    }
    if (keystates[SDL_SCANCODE_DOWN]) {
        left_paddle.SetDirection(-1);
    }
}

void Pong::Render() {
    SDL_RenderClear(renderer);

    // Draw both paddles.
    left_paddle.Render(renderer);
    right_paddle.Render(renderer);

    // Draw the ball.
    ball.Render(renderer);

    // Draw the score.
    score.Render(renderer, font);

    // Dye the game field.
    SDL_SetRenderDrawColor(renderer, BGColor.r, BGColor.g, BGColor.b, BGColor.a);

    SDL_RenderPresent(renderer);
}

void Pong::CheckCollisions() {    
    if ( SDL_HasIntersection(ball.GetPosition(), left_paddle.GetPosition()) ) {
        ball.Bounce(left_paddle.GetPosition());
    }
    else if ( SDL_HasIntersection( ball.GetPosition(), right_paddle.GetPosition()) ){
        ball.Bounce(right_paddle.GetPosition());
    }
    
}
