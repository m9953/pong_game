#pragma once
#include <SDL.h>
#include <iostream>

#include "Parameters.h"

class Paddle
{
public:
	Paddle(int x, int y);
	~Paddle() = default;

	void SetDirection(int newDir);
	SDL_Rect* GetPosition();

	void Update(double time);
	void Render(SDL_Renderer* renderer);
	void SetPositionY(int y);
	
private:
	SDL_Rect position;
	// Extra variable of real y coordinate for more precise calculations.
	double yCoord;
	SDL_Color color;

	/* Direction of paddle movement. 
		-1 - DOWN
		 0 - NO MOVEMENT
		 1 - UP
	*/
	int direction;
};
